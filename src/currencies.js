import { readable } from 'svelte/store';

export let currencies = readable(
	[
		{
			countryiso: "AF",
			currencies: [
				{
					currencyname: "Afghanistan Afghani",
					currencyiso: "AFN"
				}
			]
		},
		{
			countryiso: "XP",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AL",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				},
				{
					currencyname: "Albanian Lek",
					currencyiso: "ALL"
				}
			]
		},
		{
			countryiso: "DZ",
			currencies: [
				{
					currencyname: "Algerian Dinar",
					currencyiso: "DZD"
				}
			]
		},
		{
			countryiso: "AS",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AD",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "AO",
			currencies: [
				{
					currencyname: "Angolan  Kwanza",
					currencyiso: "AOA"
				}
			]
		},
		{
			countryiso: "AI",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				},
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "AG",
			currencies: [
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "AR",
			currencies: [
				{
					currencyname: "Argentine Peso",
					currencyiso: "ARS"
				}
			]
		},
		{
			countryiso: "AW",
			currencies: [
				{
					currencyname: "Aruban Florin",
					currencyiso: "AWG"
				}
			]
		},
		{
			countryiso: "AU",
			currencies: [
				{
					currencyname: "Australian Dollar",
					currencyiso: "AUD"
				}
			]
		},
		{
			countryiso: "AT",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "AZ",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				},
				{
					currencyname: "Azerbaijani Manat",
					currencyiso: "AZN"
				}
			]
		},
		{
			countryiso: "BS",
			currencies: [
				{
					currencyname: "Bahamian Dollar",
					currencyiso: "BSD"
				}
			]
		},
		{
			countryiso: "BH",
			currencies: [
				{
					currencyname: "Bahraini Dinar",
					currencyiso: "BHD"
				}
			]
		},
		{
			countryiso: "XB",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BD",
			currencies: [
				{
					currencyname: "Bangladeshi Taka",
					currencyiso: "BDT"
				}
			]
		},
		{
			countryiso: "BB",
			currencies: [
				{
					currencyname: "Barbados Dollar",
					currencyiso: "BBD"
				}
			]
		},
		{
			countryiso: "BE",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QQ",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BZ",
			currencies: [
				{
					currencyname: "Belize Dollar",
					currencyiso: "BZD"
				}
			]
		},
		{
			countryiso: "BJ",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "BM",
			currencies: [
				{
					currencyname: "BERMUDA",
					currencyiso: "BMD"
				}
			]
		},
		{
			countryiso: "BT",
			currencies: [
				{
					currencyname: "Bhutan Ngultrum",
					currencyiso: "BTN"
				}
			]
		},
		{
			countryiso: "BO",
			currencies: [
				{
					currencyname: "Bolivian Boliviano",
					currencyiso: "BOB"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BA",
			currencies: [
				{
					currencyname: "Convertible  Mark",
					currencyiso: "BAM"
				}
			]
		},
		{
			countryiso: "BW",
			currencies: [
				{
					currencyname: "Botswana Pula",
					currencyiso: "BWP"
				}
			]
		},
		{
			countryiso: "BR",
			currencies: [
				{
					currencyname: "Brazilian Real",
					currencyiso: "BRL"
				}
			]
		},
		{
			countryiso: "VG",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BN",
			currencies: [
				{
					currencyname: "Brunei Dollar",
					currencyiso: "BND"
				}
			]
		},
		{
			countryiso: "BG",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				},
				{
					currencyname: "Bulgarian New Lev",
					currencyiso: "BGN"
				}
			]
		},
		{
			countryiso: "BF",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "BI",
			currencies: [
				{
					currencyname: "Burundi Franc",
					currencyiso: "BIF"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "KH",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CM",
			currencies: [
				{
					currencyname: "CFA Franc BEAC",
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "CA",
			currencies: [
				{
					currencyname: "Canadian Dollar",
					currencyiso: "CAD"
				}
			]
		},
		{
			countryiso: "CV",
			currencies: [
				{
					currencyname: "Cape Verde Escudo",
					currencyiso: "CVE"
				}
			]
		},
		{
			countryiso: "KY",
			currencies: [
				{
					currencyname: "Cayman Islands Dollar",
					currencyiso: "KYD"
				}
			]
		},
		{
			countryiso: "CF",
			currencies: [
				{
					currencyname: "CFA Franc BEAC",
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "TD",
			currencies: [
				{
					currencyname: "CFA Franc BEAC",
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "CL",
			currencies: [
				{
					currencyname: "Chilean Peso",
					currencyiso: "CLP"
				}
			]
		},
		{
			countryiso: "CN",
			currencies: [
				{
					currencyname: "Chinese Yuan Renminbi",
					currencyiso: "CNY"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CO",
			currencies: [
				{
					currencyname: "Colombian Peso",
					currencyiso: "COP"
				}
			]
		},
		{
			countryiso: "KM",
			currencies: [
				{
					currencyname: "Comoros Franc",
					currencyiso: "KMF"
				}
			]
		},
		{
			countryiso: "CD",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CG",
			currencies: [
				{
					currencyname: "CFA Franc BEAC",
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "CK",
			currencies: [
				{
					currencyname: "New Zealand Dollar",
					currencyiso: "NZD"
				}
			]
		},
		{
			countryiso: "CR",
			currencies: [
				{
					currencyname: "Costa Rican Colon",
					currencyiso: "CRC"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HR",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "CU",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "QS",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AN",
			currencies: [
				{
					currencyname: "Netherlands Antilles Guilder",
					currencyiso: "ANG"
				}
			]
		},
		{
			countryiso: "CY",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "C2",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CZ",
			currencies: [
				{
					currencyname: "Czech Koruna",
					currencyiso: "CZK"
				}
			]
		},
		{
			countryiso: "DK",
			currencies: [
				{
					currencyname: "Danish Krone",
					currencyiso: "DKK"
				}
			]
		},
		{
			countryiso: "DJ",
			currencies: [
				{
					currencyname: "Djibouti Franc",
					currencyiso: "DJF"
				}
			]
		},
		{
			countryiso: "QV",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "DM",
			currencies: [
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "DO",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				},
				{
					currencyname: "Dominican Peso",
					currencyiso: "DOP"
				}
			]
		},
		{
			countryiso: "TP",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "EC",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "EG",
			currencies: [
				{
					currencyname: "Egyptian Pound",
					currencyiso: "EGP"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "SV",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GQ",
			currencies: [
				{
					currencyname: "CFA Franc BEAC",
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "ER",
			currencies: [
				{
					currencyname: "Eritrean Nakfa",
					currencyiso: "ERN"
				}
			]
		},
		{
			countryiso: "EE",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "ET",
			currencies: [
				{
					currencyname: "Ethiopian Birr",
					currencyiso: "ETB"
				}
			]
		},
		{
			countryiso: "FK",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				},
				{
					currencyname: "Falkland Islands Pound",
					currencyiso: "FKP"
				}
			]
		},
		{
			countryiso: "FJ",
			currencies: [
				{
					currencyname: "Fiji Dollar",
					currencyiso: "FJD"
				}
			]
		},
		{
			countryiso: "FI",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "FR",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "GF",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "GA",
			currencies: [
				{
					currencyname: "CFA Franc BEAC",
					currencyiso: "XAF"
				}
			]
		},
		{
			countryiso: "GM",
			currencies: [
				{
					currencyname: "Gambian Dalasi",
					currencyiso: "GMD"
				}
			]
		},
		{
			countryiso: "GE",
			currencies: [
				{
					currencyname: "Georgian Lari",
					currencyiso: "GEL"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "DE",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QO",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GH",
			currencies: [
				{
					currencyname: "Ghanaian Cedi",
					currencyiso: "GHS"
				}
			]
		},
		{
			countryiso: "GI",
			currencies: [
				{
					currencyname: "British Pound",
					currencyiso: "GBP"
				}
			]
		},
		{
			countryiso: "GR",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QZ",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GD",
			currencies: [
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "GP",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "GU",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "XY",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "GT",
			currencies: [
				{
					currencyname: "Guatemalan Quetzal",
					currencyiso: "GTQ"
				}
			]
		},
		{
			countryiso: "GN",
			currencies: [
				{
					currencyname: "Guinea Franc",
					currencyiso: "GNF"
				}
			]
		},
		{
			countryiso: "GW",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "GY",
			currencies: [
				{
					currencyname: "Guyanese Dollar",
					currencyiso: "GYD"
				}
			]
		},
		{
			countryiso: "HT",
			currencies: [
				{
					currencyname: "Haitian Gourde",
					currencyiso: "HTG"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HN",
			currencies: [
				{
					currencyname: "Honduran Lempira",
					currencyiso: "HNL"
				}
			]
		},
		{
			countryiso: "QR",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HK",
			currencies: [
				{
					currencyname: "Hong Kong Dollar",
					currencyiso: "HKD"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "HU",
			currencies: [
				{
					currencyname: "Hungarian Forint",
					currencyiso: "HUF"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IS",
			currencies: [
				{
					currencyname: "Iceland Krona",
					currencyiso: "ISK"
				},
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "XM",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IN",
			currencies: [
				{
					currencyname: "Indian Rupee",
					currencyiso: "INR"
				}
			]
		},
		{
			countryiso: "ID",
			currencies: [
				{
					currencyname: "Indonesian Rupiah",
					currencyiso: "IDR"
				}
			]
		},
		{
			countryiso: "IQ",
			currencies: [
				{
					currencyname: "Iraqi Dinar",
					currencyiso: "IQD"
				}
			]
		},
		{
			countryiso: "QX",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IE",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "IL",
			currencies: [
				{
					currencyname: "Israeli New Shekel",
					currencyiso: "ILS"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "IT",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QP",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "CI",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "JM",
			currencies: [
				{
					currencyname: "Jamaican Dollar",
					currencyiso: "JMD"
				}
			]
		},
		{
			countryiso: "JP",
			currencies: [
				{
					currencyname: "Japanese Yen",
					currencyiso: "JPY"
				}
			]
		},
		{
			countryiso: "QM",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "JO",
			currencies: [
				{
					currencyname: "Jordanian Dinar",
					currencyiso: "JOD"
				}
			]
		},
		{
			countryiso: "KZ",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				},
				{
					currencyname: "Kazakhstan Tenge",
					currencyiso: "KZT"
				}
			]
		},
		{
			countryiso: "KE",
			currencies: [
				{
					currencyname: "Kenyan Shilling",
					currencyiso: "KES"
				}
			]
		},
		{
			countryiso: "KR",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				},
				{
					currencyname: "Korean Won",
					currencyiso: "KRW"
				}
			]
		},
		{
			countryiso: "QN",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "K1",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "XF",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "KW",
			currencies: [
				{
					currencyname: "Kuwaiti Dinar",
					currencyiso: "KWD"
				}
			]
		},
		{
			countryiso: "QU",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "KG",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AA",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LA",
			currencies: [
				{
					currencyname: "Laos Kip",
					currencyiso: "LAK"
				}
			]
		},
		{
			countryiso: "LV",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "LB",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LS",
			currencies: [
				{
					currencyname: "Lesotho Loti",
					currencyiso: "LSL"
				}
			]
		},
		{
			countryiso: "LR",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LI",
			currencies: [
				{
					currencyname: "Swiss Franc",
					currencyiso: "CHF"
				}
			]
		},
		{
			countryiso: "LT",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "LU",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MO",
			currencies: [
				{
					currencyname: "Macau Pataca",
					currencyiso: "MOP"
				}
			]
		},
		{
			countryiso: "MK",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				},
				{
					currencyname: "MACEDONIAN DENAR",
					currencyiso: "MKD"
				}
			]
		},
		{
			countryiso: "MG",
			currencies: [
				{
					currencyname: "Malagasy Ariary",
					currencyiso: "MGA"
				}
			]
		},
		{
			countryiso: "MW",
			currencies: [
				{
					currencyname: "Malawi Kwacha",
					currencyiso: "MWK"
				}
			]
		},
		{
			countryiso: "MY",
			currencies: [
				{
					currencyname: "Malaysian Ringgit",
					currencyiso: "MYR"
				}
			]
		},
		{
			countryiso: "MV",
			currencies: [
				{
					currencyname: "Maldive Rufiyaa",
					currencyiso: "MVR"
				}
			]
		},
		{
			countryiso: "ML",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "MT",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MH",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "MQ",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MU",
			currencies: [
				{
					currencyname: "Mauritius Rupee",
					currencyiso: "MUR"
				}
			]
		},
		{
			countryiso: "MX",
			currencies: [
				{
					currencyname: "Mexican Peso",
					currencyiso: "MXN"
				}
			]
		},
		{
			countryiso: "FM",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "MD",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				},
				{
					currencyname: "Moldovan Leu",
					currencyiso: "MDL"
				},
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MN",
			currencies: [
				{
					currencyname: "Mongolian Tugrik",
					currencyiso: "MNT"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "ME",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "MS",
			currencies: [
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "MA",
			currencies: [
				{
					currencyname: "Moroccan Dirham",
					currencyiso: "MAD"
				}
			]
		},
		{
			countryiso: "MZ",
			currencies: [
				{
					currencyname: "Mozambique New Metical",
					currencyiso: "MZN"
				}
			]
		},
		{
			countryiso: "MM",
			currencies: [
				{
					currencyname: "Myanmar Kyat",
					currencyiso: "MMK"
				}
			]
		},
		{
			countryiso: "NA",
			currencies: [
				{
					currencyname: "Namibia Dollar",
					currencyiso: "NAD"
				}
			]
		},
		{
			countryiso: "NR",
			currencies: [
				{
					currencyname: "Australian Dollar",
					currencyiso: "AUD"
				}
			]
		},
		{
			countryiso: "NP",
			currencies: [
				{
					currencyname: "Nepalese Rupee",
					currencyiso: "NPR"
				}
			]
		},
		{
			countryiso: "NL",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "QT",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "NZ",
			currencies: [
				{
					currencyname: "New Zealand Dollar",
					currencyiso: "NZD"
				}
			]
		},
		{
			countryiso: "NI",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "NE",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "NG",
			currencies: [
				{
					currencyname: "Nigerian Naira",
					currencyiso: "NGN"
				}
			]
		},
		{
			countryiso: "NU",
			currencies: [
				{
					currencyname: "New Zealand Dollar",
					currencyiso: "NZD"
				}
			]
		},
		{
			countryiso: "MP",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "NO",
			currencies: [
				{
					currencyname: "Norwegian Krone",
					currencyiso: "NOK"
				}
			]
		},
		{
			countryiso: "OM",
			currencies: [
				{
					currencyname: "Omani Rial",
					currencyiso: "OMR"
				}
			]
		},
		{
			countryiso: "PK",
			currencies: [
				{
					currencyname: "Pakistan Rupee",
					currencyiso: "PKR"
				}
			]
		},
		{
			countryiso: "PW",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PS",
			currencies: [
				{
					currencyname: "Israeli New Shekel",
					currencyiso: "ILS"
				},
				{
					currencyname: "Jordanian Dinar",
					currencyiso: "JOD"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PA",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PG",
			currencies: [
				{
					currencyname: "Papua New Guinea Kina",
					currencyiso: "PGK"
				}
			]
		},
		{
			countryiso: "PY",
			currencies: [
				{
					currencyname: "Paraguay Guarani",
					currencyiso: "PYG"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PE",
			currencies: [
				{
					currencyname: "Sol",
					currencyiso: "PEN"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PH",
			currencies: [
				{
					currencyname: "Philippine Peso",
					currencyiso: "PHP"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "PL",
			currencies: [
				{
					currencyname: "Polish Zloty",
					currencyiso: "PLN"
				}
			]
		},
		{
			countryiso: "PT",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "XT",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "QA",
			currencies: [
				{
					currencyname: "Qatari Rial",
					currencyiso: "QAR"
				}
			]
		},
		{
			countryiso: "QY",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "RO",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				},
				{
					currencyname: "Romanian New Leu",
					currencyiso: "RON"
				}
			]
		},
		{
			countryiso: "XW",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "RW",
			currencies: [
				{
					currencyname: "Rwanda Franc",
					currencyiso: "RWF"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "BL",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "KN",
			currencies: [
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LC",
			currencies: [
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "VC",
			currencies: [
				{
					currencyname: "East Caribbean Dollar",
					currencyiso: "XCD"
				}
			]
		},
		{
			countryiso: "XU",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "WS",
			currencies: [
				{
					currencyname: "Samoan Tala",
					currencyiso: "WST"
				}
			]
		},
		{
			countryiso: "SA",
			currencies: [
				{
					currencyname: "Saudi Riyal",
					currencyiso: "SAR"
				}
			]
		},
		{
			countryiso: "SN",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "YU",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				},
				{
					currencyname: "Serbian Dinar",
					currencyiso: "RSD"
				}
			]
		},
		{
			countryiso: "SC",
			currencies: [
				{
					currencyname: "Seychelles Rupee",
					currencyiso: "SCR"
				}
			]
		},
		{
			countryiso: "SL",
			currencies: [
				{
					currencyname: "Sierra Leone New",
					currencyiso: "SLE"
				}
			]
		},
		{
			countryiso: "SG",
			currencies: [
				{
					currencyname: "Singapore Dollar",
					currencyiso: "SGD"
				}
			]
		},
		{
			countryiso: "SK",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "SI",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "SB",
			currencies: [
				{
					currencyname: "Solomon Islands Dollar",
					currencyiso: "SBD"
				}
			]
		},
		{
			countryiso: "ZA",
			currencies: [
				{
					currencyname: "South African Rand",
					currencyiso: "ZAR"
				}
			]
		},
		{
			countryiso: "ES",
			currencies: [
				{
					currencyname: "Euro",
					currencyiso: "EUR"
				}
			]
		},
		{
			countryiso: "AB",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "LK",
			currencies: [
				{
					currencyname: "Sri Lanka Rupee",
					currencyiso: "LKR"
				}
			]
		},
		{
			countryiso: "S1",
			currencies: [
				{
					currencyname: "Netherlands Antilles Guilder",
					currencyiso: "ANG"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "SD",
			currencies: [
				{
					currencyname: "Sudan Pound",
					currencyiso: "SDG"
				}
			]
		},
		{
			countryiso: "SR",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "SE",
			currencies: [
				{
					currencyname: "Swedish Krona",
					currencyiso: "SEK"
				}
			]
		},
		{
			countryiso: "CH",
			currencies: [
				{
					currencyname: "Swiss Franc",
					currencyiso: "CHF"
				}
			]
		},
		{
			countryiso: "TW",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TJ",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TZ",
			currencies: [
				{
					currencyname: "Tanzanian Shilling",
					currencyiso: "TZS"
				}
			]
		},
		{
			countryiso: "TH",
			currencies: [
				{
					currencyname: "Thai Baht",
					currencyiso: "THB"
				}
			]
		},
		{
			countryiso: "XV",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TG",
			currencies: [
				{
					currencyname: "CFA Franc BCEAO",
					currencyiso: "XOF"
				}
			]
		},
		{
			countryiso: "TO",
			currencies: [
				{
					currencyname: "Tongan Pa'anga",
					currencyiso: "TOP"
				}
			]
		},
		{
			countryiso: "TT",
			currencies: [
				{
					currencyname: "Trinidad/Tobago Dollar",
					currencyiso: "TTD"
				}
			]
		},
		{
			countryiso: "TN",
			currencies: [
				{
					currencyname: "Tunisian Dinar",
					currencyiso: "TND"
				}
			]
		},
		{
			countryiso: "TR",
			currencies: [
				{
					currencyname: "New Turkish Lira",
					currencyiso: "TRY"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "XN",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TM",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TC",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "TV",
			currencies: [
				{
					currencyname: "Australian Dollar",
					currencyiso: "AUD"
				}
			]
		},
		{
			countryiso: "XE",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "UG",
			currencies: [
				{
					currencyname: "Uganda Shilling",
					currencyiso: "UGX"
				}
			]
		},
		{
			countryiso: "UA",
			currencies: [
				{
					currencyname: "Ukraine Hryvnia",
					currencyiso: "UAH"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "AE",
			currencies: [
				{
					currencyname: "Utd. Arab Emir. Dirham",
					currencyiso: "AED"
				}
			]
		},
		{
			countryiso: "GB",
			currencies: [
				{
					currencyname: "British Pound",
					currencyiso: "GBP"
				}
			]
		},
		{
			countryiso: "QW",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "US",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "UY",
			currencies: [
				{
					currencyname: "Peso  Uruguayo",
					currencyiso: "UYU"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "UZ",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "VU",
			currencies: [
				{
					currencyname: "Vanuatu Vatu",
					currencyiso: "VUV"
				}
			]
		},
		{
			countryiso: "VE",
			currencies: [
				{
					currencyname: "Venezuelan Bolivar Soberano",
					currencyiso: "VEF"
				}
			]
		},
		{
			countryiso: "VN",
			currencies: [
				{
					currencyname: "Vietnamese Dong",
					currencyiso: "VND"
				},
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "YE",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		},
		{
			countryiso: "ZM",
			currencies: [
				{
					currencyname: "New Kwacha",
					currencyiso: "ZMW"
				}
			]
		},
		{
			countryiso: "ZW",
			currencies: [
				{
					currencyname: "US Dollar",
					currencyiso: "USD"
				}
			]
		}
	]
);

export default currencies;