import { writable } from 'svelte/store';

// Sender
export let sendIso = writable("GB");
export let sendCurnc = writable("GBP");
export let sendLang = writable("EN");

// Default Amount
export let sendAmount = writable(100);

// Receiver
export let receiveIso = writable("PK");
export let receiveCurnc = writable("PKR");

// Promo
export let payinPromo = writable("PA");

// Do not change
export let promise = writable();
export let receiveAmount = writable();
export let bestOption = writable([]);
export let hostName = writable("https://www.westernunion.com");
export let openDropdown = writable(false);
export let openSelections = writable(false);
export let isLoading = writable(false);
export let serviceGroup = writable([]);
export let payinSelected = writable();
export let payoutSelected = writable();
export let uuidv4 = writable();
export let serviceSelected = writable([]);
export let sendCountryName = writable();